/*
nvm use 12

npm install axlsign
*/
var crypto = require('crypto');
var axlsign = require('axlsign');

function generateCurve25519KeyPair() {
  var seed = crypto.randomBytes(32);
  return axlsign.generateKeyPair(seed);
}

function Uint8ToBase64(u8) {
  return Buffer.from(u8, 'binary').toString('base64')
}

function Base64ToUint8(b64) {
  return new Uint8Array(Buffer.from(b64, 'base64'));
}

function sign(sk, m) {
  return axlsign.sign(sk, m);
}

function verify(publicKey, message, signature) {
  return axlsign.verify(publicKey, message, signature);
}

console.log('Generate a key pair for Curve25519');

let curve25519Keypair = generateCurve25519KeyPair();
let privateKey = curve25519Keypair.private;
let publicKey = curve25519Keypair.public;

let b64PrivateKey = Uint8ToBase64(privateKey);
let b64PublicKey = Uint8ToBase64(publicKey);
console.log('\nbase64 encoded key data:');
console.log('encoded Curve25519 PrivateKey: ' + b64PrivateKey);
console.log('encoded Curve25519 PublicKey:  ' + b64PublicKey);

let decodedPrivateKey = Base64ToUint8(b64PrivateKey);
let decodedPublicKey = Base64ToUint8(b64PublicKey);

let message = new Uint8Array("testing");

let signature = sign(Base64ToUint8(b64PrivateKey), message);
let b64signature = Uint8ToBase64(signature);
console.log("encoded signature: " + b64signature);


let decodedSignature = Base64ToUint8(b64signature);
console.log("verify: " + verify(decodedPublicKey, message, decodedSignature));







// Test from Java
console.log("\n\nstart verify java results\n\n");

let testMessage = new Uint8Array("This is a sample string");
// Test 1
// message = "This is a simple string"
// Curve25519 PrivateKey: sKwOFbsYEMt69L2nMoIgvaEkBYZeqav+tmSa8eR0oVw=
// Curve25519 PublicKey:  W9TvlZnf6glBzHiUu0u8PMU6KLjWu2sNTq4/4hyCLB8=
// Curve25519 Signature:  vyzQubo1lsIozqdztmANmr5T+LE2pLUDkmfOyMedZlEp3pjYEV+cgy1vVG5mKqoYl7EcF7P3Ad/LWTr9krLcAg==

let test1PrivateKey = "sKwOFbsYEMt69L2nMoIgvaEkBYZeqav+tmSa8eR0oVw=";
let test1PublicKey = "W9TvlZnf6glBzHiUu0u8PMU6KLjWu2sNTq4/4hyCLB8=";
let test1SignatureJava = "vyzQubo1lsIozqdztmANmr5T+LE2pLUDkmfOyMedZlEp3pjYEV+cgy1vVG5mKqoYl7EcF7P3Ad/LWTr9krLcAg==";
let test1SignatureNode = Uint8ToBase64(sign(Base64ToUint8(test1PrivateKey), testMessage));

console.log("test 1 private key: " + test1PrivateKey);
console.log("test 1 public key: " + test1PublicKey);
console.log("test 1 signature (java): " + test1SignatureJava);
console.log("test 1 signature (node): " + test1SignatureNode);
console.log("test 1 verify (java): " + verify(Base64ToUint8(test1PublicKey), testMessage, Base64ToUint8(test1SignatureJava)));
console.log("test 1 verify (node): " + verify(Base64ToUint8(test1PublicKey), testMessage, Base64ToUint8(test1SignatureNode)));


console.log();
// Test 2
// message = "This is a simple string"
// Curve25519 PrivateKey: aJPzn+ge8yjXj/Uwa8/e+o3MqMi5/YS/bjxxz2JXK30=
// Curve25519 PublicKey:  e0Egwk326kLqTN5e2n/bgCMgG7j76tQ92zxdxDrHemk=
// Curve25519 Signature:  Dt7ebRjvUl2dQy9lJkna5AxeQUvfszHfsrBbiC2O91lGq52ALPexumjCZu4BftyqhaVNZBGWrN0eW3tv2zCACQ==
let test2PrivateKey = "aJPzn+ge8yjXj/Uwa8/e+o3MqMi5/YS/bjxxz2JXK30=";
let test2PublicKey = "e0Egwk326kLqTN5e2n/bgCMgG7j76tQ92zxdxDrHemk=";
let test2SignatureJava = "Dt7ebRjvUl2dQy9lJkna5AxeQUvfszHfsrBbiC2O91lGq52ALPexumjCZu4BftyqhaVNZBGWrN0eW3tv2zCACQ==";
let test2SignatureNode = Uint8ToBase64(sign(Base64ToUint8(test2PrivateKey), testMessage));

console.log("test 2 private key: " + test2PrivateKey);
console.log("test 2 public key: " + test2PublicKey);
console.log("test 2 signature (java): " + test2SignatureJava);
console.log("test 2 signature (node): " + test2SignatureNode);
console.log("test 2 verify (java): " + verify(Base64ToUint8(test2PublicKey), testMessage, Base64ToUint8(test2SignatureJava)));
console.log("test 2 verify (node): " + verify(Base64ToUint8(test2PublicKey), testMessage, Base64ToUint8(test2SignatureNode)));
